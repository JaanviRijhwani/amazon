<!--begin: Datatable-->
<table class="table table-bordered table-hover table-checkable" id="amazon_datatable" style="margin-top: 13px !important">
    <thead>
    <tr>
        <th>SR NO</th>
        <th>Fulfillment Type</th>
        <th>Item Type</th>
        <th>Region</th>
        <th>Customer Pays</th>
        <th>Referral Fee</th>
        <th>Closing Fee</th>
        <th>Weight Handling Fee</th>
        <th>Pick & Pack Fee</th>
        <th>Stoarge Fee</th>
        <th>GST</th>
        <th>Total Fee</th>
        <th>INR Left</th>
    </tr>
    </thead>
    <tbody>
    @foreach($final as $ele)
        <tr>
        <td>{{$ele['sr_no']}}</td>
        <td>{{$ele['fullfillment_type']}}</td>
        <td>{{$ele['item_type']}}</td>
        <td>{{$ele['zone']}}</td>
        <td>{{$ele['customer_pays']}}</td>
        <td>{{$ele['referral_fee']}}</td>
        <td>{{$ele['closing_fee']}}</td>
        <td>{{$ele['weight_fee']}}</td>
        <td>{{$ele['pick_and_pack']}}</td>
        <td>{{$ele['storage_fee']}}</td>
        <td>{{$ele['gst']}}</td>
        <td>{{$ele['total_fee']}}</td>
        <td>{{$ele['inr_left']}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<!--end: Datatable-->
