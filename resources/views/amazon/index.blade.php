<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
  <!-- Begin Page Content -->
  <div class="container-fluid">

      <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h3 mb-0 text-gray-800">Amazon</h1>
      </div>
      <div class="row">

          <div class="col-lg-12">

             <!-- Basic Card Example -->
              <div class="card shadow mb-4">

                  <div class="card-header py-3">

                      <h6 class="m-0 font-weight-bold text-primary">Amazon Calculator</h6>
                  </div>
                  <div class="card-body">
                      <div class="col-md-12">

                          <form action="{{ route('amazon-fees.store') }}" method="POST" id="amazon_calculator">
                              @csrf
                              <div class="row">
                                  @if(isset($request))
                                  <div class="form-group col-3">
                                      <label for="state">Select Platform</label>
                                    <select class="form-select form-control select2" name="platform" id="platform" required>
                                            <option value="amazon">
                                                Amazon
                                            </option>
                                    </select>
                                  </div>

                                  <div class="form-group col-3">
                                      <label for="state">Select Nodes</label>
                                      <select class="form-select form-control select2" name="nodes" id="nodes" required>
                                          @foreach(\App\Helpers\Constants\BaseConstants::NODES as $node)
                                              <option value="{{$node}}" {{ $request->nodes == $node?'selected':'' }}>{{$node}}</option>
                                          @endforeach
                                      </select>
                                  </div>

                                  <div class="form-group col-3">
                                      <label for="state">Select Step Level</label>
                                    <select class="form-select form-control select2" name="level" id="level" required>
                                        @foreach(\App\Helpers\Constants\BaseConstants::UILEVELS as $level)
                                            <option value="{{$level}}" {{ $request->level == $level?'selected':'' }}>{{$level}}</option>
                                        @endforeach
                                    </select>
                                  </div>

                                  <div class="form-group col-3">
                                    <label for="name">Listing Price</label>
                                    <input type="text"
                                        class="form-control"
                                        placeholder="Enter Listing Price"
                                        name="listing_price"
                                        id="listing_price"
                                        value="{{$request->listing_price}}"
                                        required/>
                                  </div>

                              </div>

                              <div class="row">
                                <div class="form-group col-3">
                                    <label for="name">Weight (GM) </label>
                                    <input type="text"
                                        class="form-control"
                                        placeholder="Enter Weight"
                                        name="weight"
                                        id="weight" value="{{$request->weight}}"
                                        required/>
                                  </div>

                                  <div class="form-group col-3">
                                    <label for="name">Length (CM)</label>
                                    <input type="text"
                                        class="form-control"
                                        placeholder="Enter Length"
                                        name="length"
                                        id="length" value="{{$request->length}}"
                                        required/>
                                  </div>

                                  <div class="form-group col-3">
                                    <label for="name">Width (CM) </label>
                                    <input type="text"
                                        class="form-control"
                                        placeholder="Enter Width"
                                        name="width"
                                        id="width" value="{{$request->width}}"
                                        required/>
                                  </div>

                                  <div class="form-group col-3">
                                    <label for="name">Height (CM)</label>
                                    <input type="text"
                                        class="form-control"
                                        placeholder="Enter Height"
                                        name="height"
                                        id="height" value="{{$request->height}}"
                                        required/>
                                  </div>
                              </div>

                              <div class="row">
                                  <div class="form-group col-4">
                                    <label for="name">Local Delivery Charge</label>
                                    <input type="text"
                                        class="form-control"
                                        placeholder="Enter Local Delivery Charge"
                                        name="local_delivery_charge"
                                        id="local_delivery_charge" value="{{$request->local_delivery_charge}}"
                                        required/>
                                  </div>

                                  <div class="form-group col-4">
                                    <label for="name">Regional Delivery Charge</label>
                                    <input type="text"
                                        class="form-control"
                                        placeholder="Enter Regional Delivery Charge"
                                        name="regional_delivery_charge"
                                        id="regional_delivery_charge" value="{{$request->regional_delivery_charge}}"
                                        required/>
                                  </div>

                                  <div class="form-group col-4">
                                    <label for="name">National Delivery Charge</label>
                                    <input type="text"
                                        class="form-control"
                                        placeholder="Enter National Delivery Charge"
                                        name="national_delivery_charge"
                                        id="national_delivery_charge" value="{{$request->national_delivery_charge}}"
                                        required/>
                                  </div>
                              </div>

                              @else

                                  <div class="form-group col-3">
                                      <label for="state">Select Platform</label>
                                      <select class="form-select form-control select2" name="platform" id="platform" required>
                                          <option value="amazon">
                                              Amazon
                                          </option>
                                      </select>
                                  </div>

                                  <div class="form-group col-3">
                                      <label for="state">Select Nodes</label>
                                      <select class="form-select form-control select2" name="nodes" id="nodes" required>
                                          @foreach(\App\Helpers\Constants\BaseConstants::NODES as $node)
                                              <option value="{{$node}}">{{$node}}</option>
                                          @endforeach
                                      </select>
                                  </div>

                                  <div class="form-group col-3">
                                      <label for="state">Select Step Level</label>
                                      <select class="form-select form-control select2" name="level" id="level" required>
                                          @foreach(\App\Helpers\Constants\BaseConstants::UILEVELS as $level)
                                              <option value="{{$level}}">{{$level}}</option>
                                          @endforeach
                                      </select>
                                  </div>

                                  <div class="form-group col-3">
                                      <label for="name">Listing Price</label>
                                      <input type="text"
                                             class="form-control"
                                             placeholder="Enter Listing Price"
                                             name="listing_price"
                                             id="listing_price"
                                             required/>
                                  </div>

                      </div>

                      <div class="row">
                          <div class="form-group col-3">
                              <label for="name">Weight (GM) </label>
                              <input type="text"
                                     class="form-control"
                                     placeholder="Enter Weight"
                                     name="weight"
                                     id="weight"
                                     required/>
                          </div>

                          <div class="form-group col-3">
                              <label for="name">Length (CM)</label>
                              <input type="text"
                                     class="form-control"
                                     placeholder="Enter Length"
                                     name="length"
                                     id="length"
                                     required/>
                          </div>

                          <div class="form-group col-3">
                              <label for="name">Width (CM) </label>
                              <input type="text"
                                     class="form-control"
                                     placeholder="Enter Width"
                                     name="width"
                                     id="width"
                                     required/>
                          </div>

                          <div class="form-group col-3">
                              <label for="name">Height (CM)</label>
                              <input type="text"
                                     class="form-control"
                                     placeholder="Enter Height"
                                     name="height"
                                     id="height"
                                     required/>
                          </div>
                      </div>

                      <div class="row">
                          <div class="form-group col-4">
                              <label for="name">Local Delivery Charge</label>
                              <input type="text"
                                     class="form-control"
                                     placeholder="Enter Local Delivery Charge"
                                     name="local_delivery_charge"
                                     id="local_delivery_charge"
                                     required/>
                          </div>

                          <div class="form-group col-4">
                              <label for="name">Regional Delivery Charge</label>
                              <input type="text"
                                     class="form-control"
                                     placeholder="Enter Regional Delivery Charge"
                                     name="regional_delivery_charge"
                                     id="regional_delivery_charge"
                                     required/>
                          </div>

                          <div class="form-group col-4">
                              <label for="name">National Delivery Charge</label>
                              <input type="text"
                                     class="form-control"
                                     placeholder="Enter National Delivery Charge"
                                     name="national_delivery_charge"
                                     id="national_delivery_charge"
                                     required/>
                          </div>
                      </div>
                                  @endif
                              <button type="submit" class="btn btn-primary" name="calculate_amazon" value="calculate_amazon">Submit</button>
                              <button type="submit" class="btn btn-primary" name="submit_with_excel_export" value="submit_with_excel_export">Submit With Excel Export</button>
                          </form>

                          <div class="mt-5">
                              <hr>
                            @if(isset($final))
                                  @include('amazon.partials._list')
                              @endif
                          </div>
                      </div>
                  </div>
              </div>

          </div>
      </div>
  </div>
  <!-- /.container-fluid -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
