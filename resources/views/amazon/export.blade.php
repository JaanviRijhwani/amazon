<table>
    <thead>
        <tr>
            <th>Sr No</th>
            <th>FullFillment Type</th>
            <th>Item Type</th>
            <th>Region</th>
            <th>Customer Pays</th>
            <th>Closing Fee</th>
            <th>Referral Fee</th>
            <th>Weight Handling Fee</th>
            <th>Pick and Pack Fee</th>
            <th>Storage Fee</th>
            <th>GST</th>
            <th>Total Fee</th>
            <th>INR Left </th>
        </tr>
    </thead>
    <tbody>
        @foreach($listing_price as $ele)
            <tr>
                <td>{{$ele['sr_no']}}</td>
                <td>{{$ele['fullfillment_type']}}</td>
                <td>{{$ele['item_type']}}</td>
                <td>{{$ele['zone']}}</td>
                <td>{{$ele['customer_pays']}}</td>
                <td>{{$ele['referral_fee']}}</td>
                <td>{{$ele['closing_fee']}}</td>
                <td>{{$ele['weight_fee']}}</td>
                <td>{{$ele['pick_and_pack']}}</td>
                <td>{{$ele['storage_fee']}}</td>
                <td>{{$ele['gst']}}</td>
                <td>{{$ele['total_fee']}}</td>
                <td>{{$ele['inr_left']}}</td>
            </tr>
        @endforeach
    </tbody>
</table>


