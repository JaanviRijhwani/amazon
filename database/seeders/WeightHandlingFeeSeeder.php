<?php

namespace Database\Seeders;

use App\Models\WeightHandlingFee;
use Illuminate\Database\Seeder;

class WeightHandlingFeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date=date_create("12-12-2020");
        // EASY SHIP
        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 32,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 46.5,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 68,
            'with_effect_from' => $date
        ]);



        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 32,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 46.5,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 68,
            'with_effect_from' => $date
        ]);



        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 38,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 48,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 69,
            'with_effect_from' => $date
        ]);


        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 38,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 48,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 69,
            'with_effect_from' => $date
        ]);



        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 44,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 52.5,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 73,
            'with_effect_from' => $date
        ]);


        // PREMIUM FOR STANDARD EASY SHIP
        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 16,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 21,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 26,
            'with_effect_from' => $date
        ]);



        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 13,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 18,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 24,
            'with_effect_from' => $date
        ]);



        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 10,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 11,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 15,
            'with_effect_from' => $date
        ]);

        // ADVANCED

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 16,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 21,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 26,
            'with_effect_from' => $date
        ]);



        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 13,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 18,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 24,
            'with_effect_from' => $date
        ]);



        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 10,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 11,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 15,
            'with_effect_from' => $date
        ]);

        // STANDARD

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 16,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 21,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 26,
            'with_effect_from' => $date
        ]);



        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 13,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 18,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 24,
            'with_effect_from' => $date
        ]);



        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 10,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 11,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 15,
            'with_effect_from' => $date
        ]);

        // NO_LEVEL

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 16,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 21,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 26,
            'with_effect_from' => $date
        ]);



        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 13,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 18,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 24,
            'with_effect_from' => $date
        ]);



        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 10,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 11,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 15,
            'with_effect_from' => $date
        ]);

        // BASIC

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 16,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 21,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 26,
            'with_effect_from' => $date
        ]);



        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 13,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 18,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 24,
            'with_effect_from' => $date
        ]);



        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 10,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 11,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 15,
            'with_effect_from' => $date
        ]);
        // HEAVY AND BULKY FOR EASY SHIP

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'HEAVY',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 12000,
            'min_weight_slab' => 1,
            'max_weight_slab' => 12000,
            'fee' => 175,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'HEAVY',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 12000,
            'min_weight_slab' => 1,
            'max_weight_slab' => 12000,
            'fee' => 264.5,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'HEAVY',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 12000,
            'min_weight_slab' => 1,
            'max_weight_slab' => 12000,
            'fee' => 175,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'HEAVY',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 12000,
            'min_weight_slab' => 1,
            'max_weight_slab' => 12000,
            'fee' => 264.5,
            'with_effect_from' => $date
        ]);


        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'HEAVY',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 12000,
            'min_weight_slab' => 1,
            'max_weight_slab' => 12000,
            'fee' => 181,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'HEAVY',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 12000,
            'min_weight_slab' => 1,
            'max_weight_slab' => 12000,
            'fee' => 266,
            'with_effect_from' => $date
        ]);


        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'HEAVY',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 12000,
            'min_weight_slab' => 1,
            'max_weight_slab' => 12000,
            'fee' => 181,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'HEAVY',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 12000,
            'min_weight_slab' => 1,
            'max_weight_slab' => 12000,
            'fee' => 266,
            'with_effect_from' => $date
        ]);


        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'HEAVY',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 12000,
            'min_weight_slab' => 1,
            'max_weight_slab' => 12000,
            'fee' => 187,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'HEAVY',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 12000,
            'min_weight_slab' => 1,
            'max_weight_slab' => 12000,
            'fee' => 270.5,
            'with_effect_from' => $date
        ]);

        // ALL FOR HEAVY EASY SHIP

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'HEAVY',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 12001,
            'max_weight_slab' => 9999999,
            'fee' => 4,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'HEAVY',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 12001,
            'max_weight_slab' => 9999999,
            'fee' => 5,
            'with_effect_from' => $date
        ]);

        // ADVANCED
        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'HEAVY',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 12001,
            'max_weight_slab' => 9999999,
            'fee' => 4,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'HEAVY',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 12001,
            'max_weight_slab' => 9999999,
            'fee' => 5,
            'with_effect_from' => $date
        ]);
        // STANDARD
        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'HEAVY',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 12001,
            'max_weight_slab' => 9999999,
            'fee' => 4,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'HEAVY',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 12001,
            'max_weight_slab' => 9999999,
            'fee' => 5,
            'with_effect_from' => $date
        ]);

        // NO_LEVEL

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'HEAVY',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 12001,
            'max_weight_slab' => 9999999,
            'fee' => 4,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'HEAVY',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 12001,
            'max_weight_slab' => 9999999,
            'fee' => 5,
            'with_effect_from' => $date
        ]);

        // BASIC
        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'HEAVY',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 12001,
            'max_weight_slab' => 9999999,
            'fee' => 4,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'HEAVY',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'EASY_SHIP',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 12001,
            'max_weight_slab' => 9999999,
            'fee' => 5,
            'with_effect_from' => $date
        ]);




        // FULLFILMENT BY AMAZON (FBA)

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 22,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 36.5,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 58,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'IXD',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 40.5,
            'with_effect_from' => $date
        ]);



        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 22,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 36.5,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 58,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'IXD',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 40.5,
            'with_effect_from' => $date
        ]);



        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 28,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 38,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 59,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'IXD',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 43,
            'with_effect_from' => $date
        ]);



        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 28,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 38,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 59,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'IXD',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 43,
            'with_effect_from' => $date
        ]);




        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 34,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 42.5,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 63,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'IXD',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 1,
            'max_weight_slab' => 500,
            'fee' => 47.5,
            'with_effect_from' => $date
        ]);

        // ALL FOR FBA STANDARD

        // PREMIUM
        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 16,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 21,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 26,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'IXD',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 22,
            'with_effect_from' => $date
        ]);




        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 13,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 18,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 24,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'IXD',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 18,
            'with_effect_from' => $date
        ]);




        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 10,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 11,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 15,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'zones' => 'IXD',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 12,
            'with_effect_from' => $date
        ]);

        // STANDARD

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 16,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 21,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 26,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'IXD',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 22,
            'with_effect_from' => $date
        ]);




        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 13,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 18,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 24,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'IXD',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 18,
            'with_effect_from' => $date
        ]);




        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 10,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 11,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 15,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'zones' => 'IXD',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 12,
            'with_effect_from' => $date
        ]);
        // ADVANCED
        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 16,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 21,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 26,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'IXD',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 22,
            'with_effect_from' => $date
        ]);




        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 13,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 18,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 24,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'IXD',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 18,
            'with_effect_from' => $date
        ]);




        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 10,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 11,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 15,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'zones' => 'IXD',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 12,
            'with_effect_from' => $date
        ]);
        // NO_LEVEL

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 16,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 21,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 26,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'IXD',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 22,
            'with_effect_from' => $date
        ]);




        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 13,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 18,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 24,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'IXD',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 18,
            'with_effect_from' => $date
        ]);




        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 10,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 11,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 15,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'zones' => 'IXD',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 12,
            'with_effect_from' => $date
        ]);
        // BASIC
        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 16,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 21,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 26,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'IXD',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 500,
            'min_weight_slab' => 501,
            'max_weight_slab' => 1000,
            'fee' => 22,
            'with_effect_from' => $date
        ]);




        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 13,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 18,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 24,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'IXD',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 1001,
            'max_weight_slab' => 5000,
            'fee' => 18,
            'with_effect_from' => $date
        ]);




        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 10,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 11,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'NATIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 15,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'zones' => 'IXD',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 5001,
            'max_weight_slab' => 9999999,
            'fee' => 12,
            'with_effect_from' => $date
        ]);
        // HEAVY AND BULY FBA


        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'HEAVY',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 12000,
            'min_weight_slab' => 1,
            'max_weight_slab' => 12000,
            'fee' => 160,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'HEAVY',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 12000,
            'min_weight_slab' => 1,
            'max_weight_slab' => 12000,
            'fee' => 249.5,
            'with_effect_from' => $date
        ]);


        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'HEAVY',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 12000,
            'min_weight_slab' => 1,
            'max_weight_slab' => 12000,
            'fee' => 160,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'HEAVY',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 12000,
            'min_weight_slab' => 1,
            'max_weight_slab' => 12000,
            'fee' => 249.5,
            'with_effect_from' => $date
        ]);


        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'HEAVY',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 12000,
            'min_weight_slab' => 1,
            'max_weight_slab' => 12000,
            'fee' => 166,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'HEAVY',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 12000,
            'min_weight_slab' => 1,
            'max_weight_slab' => 12000,
            'fee' => 251,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'HEAVY',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 12000,
            'min_weight_slab' => 1,
            'max_weight_slab' => 12000,
            'fee' => 166,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'HEAVY',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 12000,
            'min_weight_slab' => 1,
            'max_weight_slab' => 12000,
            'fee' => 251,
            'with_effect_from' => $date
        ]);


        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'HEAVY',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 12000,
            'min_weight_slab' => 1,
            'max_weight_slab' => 12000,
            'fee' => 172,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'HEAVY',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 12000,
            'min_weight_slab' => 1,
            'max_weight_slab' => 12000,
            'fee' => 255.5,
            'with_effect_from' => $date
        ]);

        // ALL FOR HEAVY FBA


        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'HEAVY',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 12001,
            'max_weight_slab' => 9999999,
            'fee' => 4,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'HEAVY',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 12001,
            'max_weight_slab' => 9999999,
            'fee' => 5,
            'with_effect_from' => $date
        ]);

        // STANDARD
        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'HEAVY',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 12001,
            'max_weight_slab' => 9999999,
            'fee' => 4,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'HEAVY',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 12001,
            'max_weight_slab' => 9999999,
            'fee' => 5,
            'with_effect_from' => $date
        ]);
        // ADVANCED
        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'HEAVY',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 12001,
            'max_weight_slab' => 9999999,
            'fee' => 4,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'HEAVY',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 12001,
            'max_weight_slab' => 9999999,
            'fee' => 5,
            'with_effect_from' => $date
        ]);
        // BASIC
        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'HEAVY',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 12001,
            'max_weight_slab' => 9999999,
            'fee' => 4,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'BASIC',
            'item_type' => 'HEAVY',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 12001,
            'max_weight_slab' => 9999999,
            'fee' => 5,
            'with_effect_from' => $date
        ]);
        // NO_LEVEL
        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'HEAVY',
            'zones' => 'LOCAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 12001,
            'max_weight_slab' => 9999999,
            'fee' => 4,
            'with_effect_from' => $date
        ]);

        WeightHandlingFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'HEAVY',
            'zones' => 'REGIONAL',
            'fullfillment_types' => 'FBA',
            'weight_slab_in_grams' => 1000,
            'min_weight_slab' => 12001,
            'max_weight_slab' => 9999999,
            'fee' => 5,
            'with_effect_from' => $date
        ]);
    }
}
