<?php

namespace Database\Seeders;

use App\Models\ReferralFee;
use Illuminate\Database\Seeder;

class ReferralFeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = date_create("12-12-2020");
        ReferralFee::create([
            'nodes' => '1-Books',
            'marketplace_id' => 1,
            'fee_percentage'=>2,
            'min_value' => 1,
            'max_value' => 250,
            'with_effect_from'=> $date
        ]);

        ReferralFee::create([
            'nodes' => '1-Books',
            'marketplace_id' => 1,
            'fee_percentage'=>4,
            'min_value' => 251,
            'max_value' => 500,
            'with_effect_from'=> $date
        ]);

        ReferralFee::create([
            'nodes' => '1-Books',
            'marketplace_id' => 1,
            'fee_percentage'=>9,
            'min_value' => 501,
            'max_value' => 1000,
            'with_effect_from'=> $date
        ]);

        ReferralFee::create([
            'nodes' => '1-Books',
            'marketplace_id' => 1,
            'fee_percentage'=>13,
            'min_value' => 1001,
            'max_value' => 9999999,
            'with_effect_from'=> $date
        ]);

        ReferralFee::create([
            'nodes' => '2-Toys',
            'marketplace_id' => 1,
            'fee_percentage'=>9.5,
            'min_value' => 1,
            'max_value' => 9999999,
            'with_effect_from'=> $date
        ]);

        ReferralFee::create([
            'nodes' => '3-Home Furnishing',
            'marketplace_id' => 1,
            'fee_percentage'=>12,
            'min_value' => 1,
            'max_value' => 9999999,
            'with_effect_from'=> $date
        ]);
    }
}
