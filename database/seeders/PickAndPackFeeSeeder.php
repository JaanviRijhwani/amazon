<?php

namespace Database\Seeders;

use App\Models\PickAndPackFee;
use Illuminate\Database\Seeder;

class PickAndPackFeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = date_create("12-12-2020");
        $pickAndPack = PickAndPackFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'fullfillment_type' => 'FBA',
            'fee' => 10,
            'with_effect_from' => $date
        ]);
        $pickAndPack = PickAndPackFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'HEAVY',
            'fullfillment_type' => 'FBA',
            'fee' => 20,
            'with_effect_from' => $date
        ]);
        $pickAndPack = PickAndPackFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'fullfillment_type' => 'FBA',
            'fee' => 10,
            'with_effect_from' => $date
        ]);
        $pickAndPack = PickAndPackFee::create([
            'levels' => 'BASIC',
            'item_type' => 'HEAVY',
            'fullfillment_type' => 'FBA',
            'fee' => 20,
            'with_effect_from' => $date
        ]);
        $pickAndPack = PickAndPackFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'fullfillment_type' => 'FBA',
            'fee' => 10,
            'with_effect_from' => $date
        ]);
        $pickAndPack = PickAndPackFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'HEAVY',
            'fullfillment_type' => 'FBA',
            'fee' => 20,
            'with_effect_from' => $date
        ]);
        $pickAndPack = PickAndPackFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'fullfillment_type' => 'FBA',
            'fee' => 10,
            'with_effect_from' => $date
        ]);
        $pickAndPack = PickAndPackFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'HEAVY',
            'fullfillment_type' => 'FBA',
            'fee' => 20,
            'with_effect_from' => $date
        ]);
        $pickAndPack = PickAndPackFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'fullfillment_type' => 'FBA',
            'fee' => 10,
            'with_effect_from' => $date
        ]);
        $pickAndPack = PickAndPackFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'HEAVY',
            'fullfillment_type' => 'FBA',
            'fee' => 20,
            'with_effect_from' => $date
        ]);

        //SELLER
        $pickAndPack = PickAndPackFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'STANDARD',
            'fullfillment_type' => 'SELLER_FLEX',
            'fee' => 10,
            'with_effect_from' => $date
        ]);
        $pickAndPack = PickAndPackFee::create([
            'levels' => 'NO_LEVEL',
            'item_type' => 'HEAVY',
            'fullfillment_type' => 'SELLER_FLEX',
            'fee' => 10,
            'with_effect_from' => $date
        ]);
        $pickAndPack = PickAndPackFee::create([
            'levels' => 'BASIC',
            'item_type' => 'STANDARD',
            'fullfillment_type' => 'SELLER_FLEX',
            'fee' => 10,
            'with_effect_from' => $date
        ]);
        $pickAndPack = PickAndPackFee::create([
            'levels' => 'BASIC',
            'item_type' => 'HEAVY',
            'fullfillment_type' => 'SELLER_FLEX',
            'fee' => 10,
            'with_effect_from' => $date
        ]);
        $pickAndPack = PickAndPackFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'STANDARD',
            'fullfillment_type' => 'SELLER_FLEX',
            'fee' => 10,
            'with_effect_from' => $date
        ]);
        $pickAndPack = PickAndPackFee::create([
            'levels' => 'PREMIUM',
            'item_type' => 'HEAVY',
            'fullfillment_type' => 'SELLER_FLEX',
            'fee' => 10,
            'with_effect_from' => $date
        ]);
        $pickAndPack = PickAndPackFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'STANDARD',
            'fullfillment_type' => 'SELLER_FLEX',
            'fee' => 10,
            'with_effect_from' => $date
        ]);
        $pickAndPack = PickAndPackFee::create([
            'levels' => 'STANDARD',
            'item_type' => 'HEAVY',
            'fullfillment_type' => 'SELLER_FLEX',
            'fee' => 10,
            'with_effect_from' => $date
        ]);
        $pickAndPack = PickAndPackFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'STANDARD',
            'fullfillment_type' => 'SELLER_FLEX',
            'fee' => 10,
            'with_effect_from' => $date
        ]);
        $pickAndPack = PickAndPackFee::create([
            'levels' => 'ADVANCED',
            'item_type' => 'HEAVY',
            'fullfillment_type' => 'SELLER_FLEX',
            'fee' => 10,
            'with_effect_from' => $date
        ]);
    }
}
