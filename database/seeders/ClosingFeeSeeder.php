<?php

namespace Database\Seeders;

use App\Models\ClosingFee;
use Illuminate\Database\Seeder;

class ClosingFeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = date_create("12-12-2020");
        ClosingFee::create([
            'marketplace_id' => 1,
            'min_value' => 0,
            'max_value' => 250,
            'fee'=>25,
            'fee_for_exception_category'=>12,
            'fullfillment_types'=>'FBA',
            'with_effect_from'=> $date
        ]);

        ClosingFee::create([
            'marketplace_id' => 1,
            'min_value' => 251,
            'max_value' => 500,
            'fee'=>20,
            'fee_for_exception_category'=>12,
            'fullfillment_types'=>'FBA',
            'with_effect_from'=> $date
        ]);

        ClosingFee::create([
            'marketplace_id' => 1,
            'min_value' => 501,
            'max_value' => 1000,
            'fee'=>15,
            'fee_for_exception_category'=>15,
            'fullfillment_types'=>'FBA',
            'with_effect_from'=> $date
        ]);

        ClosingFee::create([
            'marketplace_id' => 1,
            'min_value' => 1001,
            'max_value' => 9999999,
            'fee'=>30,
            'fee_for_exception_category'=>30,
            'fullfillment_types'=>'FBA',
            'with_effect_from'=> $date
        ]);

        ClosingFee::create([
            'marketplace_id' => 1,
            'min_value' => 0,
            'max_value' => 250,
            'fee'=>5,
            'fee_for_exception_category'=>0,
            'fullfillment_types'=>'EASY_SHIP_STANDARD',
            'with_effect_from'=> $date
        ]);

        ClosingFee::create([
            'marketplace_id' => 1,
            'min_value' => 251,
            'max_value' => 500,
            'fee'=>8,
            'fee_for_exception_category'=>0,
            'fullfillment_types'=>'EASY_SHIP_STANDARD',
            'with_effect_from'=> $date
        ]);

        ClosingFee::create([
            'marketplace_id' => 1,
            'min_value' => 501,
            'max_value' => 1000,
            'fee'=>28,
            'fee_for_exception_category'=>0,
            'fullfillment_types'=>'EASY_SHIP_STANDARD',
            'with_effect_from'=> $date
        ]);

        ClosingFee::create([
            'marketplace_id' => 1,
            'min_value' => 1001,
            'max_value' => 9999999,
            'fee'=>50,
            'fee_for_exception_category'=>0,
            'fullfillment_types'=>'EASY_SHIP_STANDARD',
            'with_effect_from'=> $date
        ]);

        ClosingFee::create([
            'marketplace_id' => 1,
            'min_value' => 0,
            'max_value' => 250,
            'fee'=>8,
            'fee_for_exception_category'=>0,
            'fullfillment_types'=>'EASY_SHIP_PRIME',
            'with_effect_from'=> $date
        ]);

        ClosingFee::create([
            'marketplace_id' => 1,
            'min_value' => 251,
            'max_value' => 500,
            'fee'=>11,
            'fee_for_exception_category'=>0,
            'fullfillment_types'=>'EASY_SHIP_PRIME',
            'with_effect_from'=> $date
        ]);

        ClosingFee::create([
            'marketplace_id' => 1,
            'min_value' => 501,
            'max_value' => 1000,
            'fee'=>25,
            'fee_for_exception_category'=>0,
            'fullfillment_types'=>'EASY_SHIP_PRIME',
            'with_effect_from'=> $date
        ]);

        ClosingFee::create([
            'marketplace_id' => 1,
            'min_value' => 1001,
            'max_value' => 9999999,
            'fee'=>45,
            'fee_for_exception_category'=>0,
            'fullfillment_types'=>'EASY_SHIP_PRIME',
            'with_effect_from'=> $date
        ]);

        ClosingFee::create([
            'marketplace_id' => 1,
            'min_value' => 0,
            'max_value' => 250,
            'fee'=>6,
            'fee_for_exception_category'=>0,
            'fullfillment_types'=>'SELF_SHIP',
            'with_effect_from'=> $date
        ]);

        ClosingFee::create([
            'marketplace_id' => 1,
            'min_value' => 251,
            'max_value' => 500,
            'fee'=>16,
            'fee_for_exception_category'=>0,
            'fullfillment_types'=>'SELF_SHIP',
            'with_effect_from'=> $date
        ]);

        ClosingFee::create([
            'marketplace_id' => 1,
            'min_value' => 501,
            'max_value' => 1000,
            'fee'=>32,
            'fee_for_exception_category'=>0,
            'fullfillment_types'=>'SELF_SHIP',
            'with_effect_from'=> $date
        ]);

        ClosingFee::create([
            'marketplace_id' => 1,
            'min_value' => 1001,
            'max_value' => 9999999,
            'fee'=>59,
            'fee_for_exception_category'=>0,
            'fullfillment_types'=>'SELF_SHIP',
            'with_effect_from'=> $date
        ]);

        ClosingFee::create([
            'marketplace_id' => 1,
            'min_value' => 0,
            'max_value' => 250,
            'fee'=>8,
            'fee_for_exception_category'=>0,
            'fullfillment_types'=>'SELLER_FLEX',
            'with_effect_from'=> $date
        ]);

        ClosingFee::create([
            'marketplace_id' => 1,
            'min_value' => 251,
            'max_value' => 500,
            'fee'=>11,
            'fee_for_exception_category'=>0,
            'fullfillment_types'=>'SELLER_FLEX',
            'with_effect_from'=> $date
        ]);

        ClosingFee::create([
            'marketplace_id' => 1,
            'min_value' => 501,
            'max_value' => 1000,
            'fee'=>25,
            'fee_for_exception_category'=>0,
            'fullfillment_types'=>'SELLER_FLEX',
            'with_effect_from'=> $date
        ]);

        ClosingFee::create([
            'marketplace_id' => 1,
            'min_value' => 1001,
            'max_value' => 9999999,
            'fee'=>45,
            'fee_for_exception_category'=>0,
            'fullfillment_types'=>'SELLER_FLEX',
            'with_effect_from'=> $date
        ]);
    }
}
