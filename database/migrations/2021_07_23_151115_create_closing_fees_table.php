<?php

use App\Helpers\Constants\BaseConstants;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClosingFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('closing_fees', function (Blueprint $table) {
            $table->id();
            $table->integer('marketplace_id');
            $table->integer('min_value');
            $table->integer('max_value');
            $table->unsignedFloat('fee');
            $table->unsignedFloat('fee_for_exception_category');
            $table->enum('fullfillment_types', BaseConstants::FULLFILLMENT_TYPE_CLOSING);
            $table->date('with_effect_from');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('closing_fees');
    }
}
