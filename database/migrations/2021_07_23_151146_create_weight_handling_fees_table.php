<?php

use App\Helpers\Constants\BaseConstants;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeightHandlingFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weight_handling_fees', function (Blueprint $table) {
            $table->id();
            $table->enum('levels', BaseConstants::LEVELS);
            $table->enum('fullfillment_types', BaseConstants::WEIGHT_HANDLING_FULLFILLMENT_TYPE);
            $table->enum('item_type', BaseConstants::ITEM_TYPE);
            $table->enum('zones', BaseConstants::ZONE);
            $table->integer('weight_slab_in_grams');
            $table->integer('min_weight_slab');
            $table->integer('max_weight_slab');
            $table->unsignedFloat('fee');
            $table->date('with_effect_from');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weight_handling_fees');
    }
}
