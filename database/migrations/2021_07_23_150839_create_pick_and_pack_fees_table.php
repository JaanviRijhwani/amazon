<?php

use App\Helpers\Constants\BaseConstants;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePickAndPackFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pick_and_pack_fees', function (Blueprint $table) {
            $table->id();
            $table->enum('levels', BaseConstants::LEVELS);
            $table->enum('item_type', BaseConstants::ITEM_TYPE);
            $table->enum('fullfillment_type', BaseConstants::FULLFILLMENT_TYPE_PICK_AND_PACK);
            $table->float('fee');
            $table->date('with_effect_from');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pick_and_pack_fees');
    }
}
