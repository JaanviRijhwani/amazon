<?php

use App\Helpers\Constants\BaseConstants;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferralFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referral_fees', function (Blueprint $table) {
            $table->id();
            $table->enum('nodes', BaseConstants::NODES);
            $table->integer('marketplace_id');
            $table->float('fee_percentage')->comment("In Percentage");
            $table->integer('min_value');
            $table->integer('max_value');
            $table->date('with_effect_from');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referral_fees');
    }
}
