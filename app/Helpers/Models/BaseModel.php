<?php
namespace App\Helpers\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    public $timestamps = true;

    /**
     * Shouldn't ever be used, but this language allows it soo...
     * @return array
     */
    public abstract static function getCreateValidationRules() : array;
    public abstract static function getUpdateValidationRules() : array;
}
