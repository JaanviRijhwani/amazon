<?php

namespace App\Helpers\Constants;


interface BaseConstants
{
    const FULLFILLMENT_TYPE_CLOSING = ['FBA', 'EASY_SHIP_PRIME', 'EASY_SHIP_STANDARD', 'SELF_SHIP', 'SELLER_FLEX'];
    const FULLFILLMENT_TYPE_PICK_AND_PACK = ['FBA', 'SELLER_FLEX'];
    const ITEM_TYPE =['STANDARD', 'HEAVY'];
    const STORAGE_FEE = 20;
    const NODES = ['1-Books', '2-Toys', '3-Home Furnishing'];
    const LEVELS = ['PREMIUM', 'ADVANCED', 'STANDARD', 'BASIC', 'NO_LEVEL'];
    const UILEVELS = ['PREMIUM', 'ADVANCED', 'STANDARD', 'BASIC', 'NO_LEVEL'];
    const WEIGHT_HANDLING_FULLFILLMENT_TYPE = ['FBA', 'EASY_SHIP'];
    const ZONE = ['LOCAL', 'REGIONAL', 'NATIONAL', 'IXD'];
}

