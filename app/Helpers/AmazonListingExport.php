<?php


namespace App\Helpers;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class AmazonListingExport implements FromView
{
    private $arr = [];
    public function __construct($arr)
    {
        $this->arr = $arr;

    }
    public function view(): View
    {
        return view('amazon.export', [
            'listing_price' => $this->arr
        ]);
    }
}
