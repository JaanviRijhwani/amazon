<?php


namespace App\Helpers;


use Illuminate\Support\Collection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class BaseExport implements FromView, WithStyles
{
    private Collection $collection;
    private array $columnSizes;
    private string $viewName;
    private array $rowStyles;

    /**
     * BrandExport constructor.
     */
    public function __construct(Collection $collection, array $columnSizes, array $rowStyles, string $viewName)
    {
        $this->collection = $collection;
        $this->columnSizes = $columnSizes;
        $this->rowStyles = $rowStyles;
        $this->viewName = $viewName;
    }

    /**
     * @param Worksheet $sheet
     * @return array
     */
    public function styles(Worksheet $sheet): array
    {
        foreach ($this->columnSizes as $columnPos => $columnSize){
            $columnDimension = $sheet->getColumnDimensionByColumn($columnPos);
            if ($columnSize == 'auto'){
                $columnDimension->setAutoSize(true);
            } else {
                $columnDimension->setWidth($columnSize);
            }
        }

        return $this->rowStyles;
    }

    /**
     * @return View
     */
    public function view(): View
    {
        return view($this->viewName,
            ['collection' => $this->collection]
        );
    }
}
