<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;

trait JsonResponseHelper
{
    private function successResponse($data, $code): JsonResponse
    {
        return response()->json($data, $code);
    }

    protected function error($message, $code = 422): JsonResponse
    {
        return response()->json(['errors' => $message, 'code' => $code], $code);
    }

    protected function success(array $data, $code = 200): JsonResponse
    {
        return $this->successResponse(['data' => $data, 'code' => $code], $code);
    }

    public function getDynamicData(
        $model,
        $searchByColumn = null,
        $searchValue = null,
        $optionDisplayColumn = 'name',
        $optionValueColumn = 'id'
    ) {
        $html = "<option value=''>Select Option</option>";
        $response_data = new $model;
        if (!empty($searchByColumn)) {
            $response_data = $response_data->where($searchByColumn, $searchValue);
        }
        $response_data = $response_data->get(["$optionDisplayColumn as name", "$optionValueColumn as id"]);
        if (!empty($response_data[0])) {
            foreach ($response_data as $data) {
                $html .= "<option value=" . $data->id . ">" . $data->name . "</option>";
            }
        }

        return $html;
    }
}
