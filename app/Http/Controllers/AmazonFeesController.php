<?php

namespace App\Http\Controllers;

use App\Helpers\AmazonListingExport;
use App\Helpers\BaseExport;
use App\Helpers\Constants\BaseConstants;
use App\Models\ClosingFee;
use App\Models\PickAndPackFee;
use App\Models\ReferralFee;
use App\Models\WeightHandlingFee;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class AmazonFeesController extends Controller
{
    public function index()
    {
        return view('amazon.index');
    }

    public function store(Request $request)
    {
        $length = $request->length;
        $width = $request->width;
        $height = $request->height;
        $weight = $request->weight;
        $nodes = $request->nodes;
        $zones_local = [
            'LOCAL' => $request->local_delivery_charge,
            'REGIONAL' => $request->regional_delivery_charge,
            'NATIONAL' => $request->national_delivery_charge,
        ];
        $level = $request->level;
        $listing_price = $request->listing_price;
        $volumemetricWeight = $this->calculateVolumemetricWeight($length, $height, $width);
        $weightFeeToBeCalculatedOn = 0;
        if($weight > $volumemetricWeight){
            $weightFeeToBeCalculatedOn = $weight;
        }else{
            $weightFeeToBeCalculatedOn = $volumemetricWeight;
        }
        $item_type = "STANDARD";
        $girth = $this->calculateGirthWeight($length, $width, $height);
        if($girth > 300 || $weightFeeToBeCalculatedOn > 22500 || max($length, $width, $height) > 72){
            $item_type = "HEAVY";
        }
        $final = [];
        $count = 1;
        foreach (BaseConstants::FULLFILLMENT_TYPE_CLOSING as $fullfillment_type) {
            foreach (BaseConstants::ZONE as $zone) {
                $listing_price_NEW = $listing_price;
                if(array_key_exists($zone, $zones_local)){
                    $listing_price_NEW += $zones_local[$zone];
                }
                $fullfillment = $fullfillment_type;
                if($fullfillment_type == 'EASY_SHIP_STANDARD'  || $fullfillment_type == 'EASY_SHIP_PRIME'){
                    $fullfillment = "EASY_SHIP";
                }
                if($fullfillment_type == 'SELLER_FLEX'){
                    $fullfillment = "FBA";
                }
                if(($fullfillment == "EASY_SHIP" && $zone == "IXD")  || ($fullfillment_type == "SELF_SHIP" && $zone == "IXD") || ($fullfillment_type == "SELLER_FLEX" && $zone == "IXD")) {
                    continue;
                }
                $weightHandlingFee = $this->weightCalc($weightFeeToBeCalculatedOn, $zone, $fullfillment, $item_type, $level);
                $referral_fee = $this->calculateReferralFee($listing_price_NEW, $nodes);
                $closing_fee = $this->calculateClosingFee($listing_price_NEW, $fullfillment_type, $nodes, $zone);
                $pickAndPack = 0;
                if($fullfillment_type == "FBA" || $fullfillment_type == 'SELLER_FLEX'){
                    $pickAndPack = $this->calculcatePickAndPack($item_type, $level, $fullfillment_type);
                }
                $storage_fee = 0;
                if($fullfillment_type == 'FBA') {
                    $storage_fee = $this->calculateStorageFee($length, $width, $height);
                }
                $total = $referral_fee + $closing_fee + $weightHandlingFee + $pickAndPack + $storage_fee;
                $gst = (($total*18)/100);
                $totalWithGST = ($total + $gst);
                $inrLeft = $listing_price_NEW - $totalWithGST;
                $final[] = [
                    'sr_no' => $count++,
                    'fullfillment_type' => $fullfillment_type,
                    'item_type' => $item_type,
                    'zone' => $zone,
                    'referral_fee' => $referral_fee,
                    'customer_pays' => $listing_price_NEW,
                    'closing_fee' => $closing_fee,
                    'weight_fee' => ($weightHandlingFee>0) ? $weightHandlingFee : "NA",
                    'pick_and_pack' => $pickAndPack,
                    'storage_fee' => round($storage_fee, 2),
                    'gst' => round($gst, 2),
                    'total_fee' => round($totalWithGST, 2),
                    'inr_left' => round($inrLeft, 2)
                ];
            }
        }

        if(isset($request->submit_with_excel_export))
        {
            return Excel::download(new AmazonListingExport($final), 'amazon_listing.xls');
        }
        return view('amazon.index', [
            'final' => $final,
            'request' => $request
        ]);
    }

    public function getExcelReport($final): BinaryFileResponse
    {
        return Excel::download(new BaseExport(
            $this->getCollectionForExport($final),
            $this->getColumnSizesForExport(),
            $this->getRowStylesForExport(),
            'amazon.export',
        ), 'amazon-fees.xlsx');
    }
    private function calculcatePickAndPack($item_type, $level, $fullfillment_type){
        $result = PickAndPackFee::where('item_type', $item_type)
                                ->where('levels', $level)
                                ->where('fullfillment_type', $fullfillment_type)
                                ->latest('with_effect_from')->first();
        return $result->fee;
    }

    private function calculateStorageFee($length, $width, $height)
    {
        $cubic = $length * $width * $height;
        $cubic_foot = $cubic/28317;
        return $cubic_foot * BaseConstants::STORAGE_FEE;
    }

    private function weightCalc($weight, $zone, $fullfillment_type, $item_type, $level)
    {
        $result = WeightHandlingFee::where('item_type', $item_type)
            ->where('fullfillment_types', $fullfillment_type)
            ->where('levels', $level)
            ->where('zones', $zone)
            ->orderBy('max_weight_slab', 'asc')
            ->latest('with_effect_from')->get();
        $weight_applied = 0;
        $weight_fee = 0;
        foreach($result as $fee){
            $count = 0;
            $divided_weight = $fee->weight_slab_in_grams;
            if($weight <= $weight_applied){
                break;
            }
            $ceilOfMax = ceil(($fee->max_weight_slab-$weight_applied) / $divided_weight);
            $ceilOfWeight = ceil(($weight-$weight_applied)/$divided_weight);
            if($ceilOfWeight < $ceilOfMax){
                $count = $ceilOfWeight;
            }else{
                $count = $ceilOfMax;
            }
            for($i = 0; $i<$count; $i++){
                $weight_applied += $fee->weight_slab_in_grams;
                $weight_fee += $fee->fee;
            }
        }
        return $weight_fee;
    }
    private function weightFeesToBeCalculateOn($weight, $volumemetricWeight)
    {
        $weightFeeToBeCalculatedOn = 0;
        if($weight > $volumemetricWeight){
            $weightFeeToBeCalculatedOn = $weight;
        }else{
            $weightFeeToBeCalculatedOn = $volumemetricWeight;
        }
    }

    private function calculateClosingFee($listing_price, $fullfillment_type, $nodes, $zone)
    {
        $result = ClosingFee::where('min_value','<=', $listing_price)
                            ->where('max_value','>=', $listing_price)
                            ->where('fullfillment_types', $fullfillment_type)
                            ->latest('with_effect_from')
                            ->first();

        if(($nodes == '1-Books' && $fullfillment_type == 'FBA') || ($nodes == '3-Home Furnishing' && $fullfillment_type == 'FBA'))
        {
            $fee = $result->fee_for_exception_category;

        }
        else if($fullfillment_type=='EASY_SHIP_STANDARD' || $fullfillment_type == 'SELLER_FLEX'|| $fullfillment_type == 'EASY_SHIP_PRIME')
        {
            $fee = $result->fee;
        }
        else {
            $fee = $result->fee;
        }
        return $fee;
    }

    private function calculateReferralFee($listing_price, $nodes)
    {
        $result = ReferralFee::where('min_value','<=', $listing_price)
                            ->where('max_value','>=', $listing_price)
                            ->where('nodes', $nodes)
                            ->latest('with_effect_from')
                            ->first();
        $referral_fee = ($result->fee_percentage * ($listing_price)) / 100;
        return $referral_fee;
    }
    private function calculateVolumemetricWeight($length, $width, $height)
    {
        return (($length * $height * $width)/5000);
    }

    private function calculateGirthWeight($length, $width, $height)
    {
        return $length + (2*($width + $height));
    }

    /**
     * @return array
     */
    private function getColumnSizesForExport(): array
    {
        return [
            2 => 20,
            3 => 20,
            4 => 20
        ];
    }

    /**
     * @return array
     */
    private function getRowStylesForExport(): array
    {
        return [
            1 => ['font' => ['bold' => true]],
        ];
    }

    public function getCollectionForExport($final): Collection
    {
        $collection = new Collection();
        foreach($final as $a)
        {
            array_push($collection, $a);
        }
        return $collection;
    }
}
