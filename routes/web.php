<?php

use App\Http\Controllers\AmazonFeesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post("/amazon-fees/excel", [AmazonFeesController::class, 'getExcelReport'])
    ->name("amazon-fees.getExcel");
Route::post("/amazon-fees/ajax", [AmazonFeesController::class, 'getAmazonFeesJson'])
    ->name("amazon-fees.getAmazonFeesJson");
Route::resource("/amazon-fees", AmazonFeesController::class);
