var initHsnCodeDataTable = function(route, csrf_token) {
    $('#hsn_code_datatable').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: route,
            type: "POST",
            data: {
                "_token": csrf_token,
            }
        },
        columns: [
            // {
            //     data: 'id',
            //     name: 'id',
            // },
            // {
            //     data: 'hsn_code',
            //     name: 'hsn_code',
            // },
            // {
            //     data: 'cgst',
            //     name: 'cgst',
            // },
            // {
            //     data: 'sgst',
            //     name: 'sgst',
            // },
            // {
            //     data: 'igst',
            //     name: 'igst',
            // },
            // {
            //     data: 'with_effect_from',
            //     name: 'with_effect_from',
            // },
        ],
        columnDefs: [
            {
                targets: 0,
                orderable: false,
                searchable: false,
            },
        ],

    });


};
